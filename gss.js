var jfaker = require('json-faker');
var faker = require('faker');
var regions = ["Ashanti","Greater Accra"];
var districts = {
    "Ashanti": ["Adansi South", "Adansi South"],
    "Greater Accra": ["Ablekuma", "Ada East"]
}
var randonRegion = Math.floor(Math.random() * Math.floor(regions.length));
var randomDistrict = Math.floor(Math.random() * Math.floor(districts[regions[randonRegion]].length))

let template = {
    "UUID": "8f4b4b93-6480-4141-9884-4de810f7f172",
    "device": {
      "apiLevel": 22,
      "app_version": "v1.1.75-Demo",
      "batterylevel": 71.0,
      "brand": "samsung",
      "freeDisk": "4,917MB",
      "freeMemory": "432MB",
      "imei_number": "352948092905516",
      "locale": "en_GB",
      "locationStatus": "Enabled",
      "manufacturer": "samsung",
      "model": "SM-T285",
      "networkAccess": "wifi",
      "orientation": "Portrait",
      "osVersion": "5.1.1",
      "osVersionName": "LOLLIPOP_MR1"
    },
    "end_date": "2019-07-30 17:03:45",
    "formID": "11032",
    "geofence": "",
    "gis": false,
    "gis_data": "",
    "label": {
      "label1": "03",
      "label2": "no_res_!@#$%"
    },
    "original_gis_data": "",
    "questions": [
      {
        "id": "1811271121715-4118",
        "type": "multi-text",
        "value": faker.address.streetAddress(true)
      },
      {
        "id": "1907222036486-4118",
        "type": "text",
        "value": faker.helpers.randomize(['A','S','D','F','G'])+faker.helpers.randomize(['A','S','D','F','G'])+"-"+faker.random.number({min:100,max:999})+"-"+faker.random.number({min:1000,max:9999})
      },
      {
        "id": "1811271146097-4118",
        "type": "number",
        "value": faker.helpers.randomize(['024','054','055','020','050']) + faker.random.number({min:1000000, max:9999999})
      },
      {
        "id": "1811271131488-4118",
        "type": "number",
        "value": faker.helpers.randomize(['024','054','055','020','050']) + faker.random.number({min:1000000, max:9999999})
      },
      {
        "id": "1811271141620-4118",
        "type": "option-tree",
        "value": districts[regions[randonRegion]][randomDistrict],
        "values":[ {
            "heading_id": "1811271141620-4118-0",
            "id": "1561633716957-9",
            "key": regions[randonRegion],
            "level": 1,
            "name": regions[randonRegion]
          },
          {
            "heading_id": "1811271141620-4118-1",
            "id": "1561633716957-9-1561633716957-2",
            "key": districts[regions[randonRegion]][randomDistrict],
            "level": 2,
            "name": districts[regions[randonRegion]][randomDistrict]
          }]
    
      },
      {
        "id": "1907261558769-4118",
        "type": "single-select",
        "value": "Type 3",
        "valueProps": {
    "key": faker.helpers.randomize(['Type 1','Type 2','Type 3']),
    "level": 0,
    "value": "Type 3"
  }
      },
      {
        "id": "1811271111355-4118",
        "type": "text",
        "value": faker.random.number({min:10,max:99})
      },
      {
        "id": "1907241811782-4118",
        "type": "number",
        "value": faker.random.number({min:100,max:999})
      },
      {
        "id": "1907052023733-4118",
        "type": "single-select",
        "value": "Occupied housing unit",
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": "Occupied housing unit"
  }
      },
      {
        "id": "1907261413321-4118",
        "type": "number",
        "value": faker.random.number({min:100,max:999})
      },
      {
        "id": "1907082121107-4118",
        "type": "number",
        "value": faker.random.number({min:10,max:99})
      },
      {
        "id": "1907240909625-4118",
        "type": "gis",
        "value": faker.address.longitude() + faker.address.latitude() + faker.random.number({min:10,max:15})
      },
      {
        "id": "1907290827029-4118",
        "type": "number",
        "value": "2"
      },
      {
        "id": "1907052100650-4118",
        "type": "number",
        "value": "1"
      },
      {
        "id": "1907071145131-4118",
        "type": "number",
        "value": "1"
      },
      {
        "id": "1907071152192",
        "type": "number",
        "value": "1"
      },
      {
        "id": "1907071157304",
        "type": "number",
        "value": "1"
      },
      {
        "id": "1907071140561",
        "type": "number",
        "value": "1"
      },
      {
        "id": "1907071107856",
        "type": "number",
        "value": "1"
      },
      {
        "id": "1907071151393-4118",
        "type": "calculation",
        "value": "4.0"
      },
      {
        "id": "1811271114013-4118",
        "type": "sub-form",
        "value": "1",
        "values": [
    {
      "UUID": "c5c8b510-2b50-4f9f-9416-cf3f61508a1d",
      "label": {
        "label1": "Aisha bee",
        "label2": "Parent/Parent in-law"
      },
      "questions": [
        {
          "id": "1811271103831-1643",
          "type": "text",
          "value": "Aisha bee"
        },
        {
          "id": "1811271242384-1643",
          "type": "single-select",
          "value": "Parent/Parent in-law",
          "valueProps": {
            "key": "Parent/Parent in-law",
            "level": 0,
            "value": "Parent/Parent in-law"
          }
        },
        {
          "id": "1811271240295-1643",
          "type": "single-select",
          "value": "Male",
          "valueProps": {
            "key": "Male",
            "level": 0,
            "value": "Male"
          }
        },
        {
          "id": "1812041627200-1643",
          "type": "single-select",
          "value": "Visitor(s) present on census night",
          "valueProps": {
            "key": "B",
            "level": 0,
            "value": "Visitor(s) present on census night"
          }
        },
        {
          "id": "1907071258306-4118",
          "type": "date",
          "value": "2010-05-01"
        },
        {
          "id": "1907071227083-4118",
          "type": "number",
          "value": "9"
        },
        {
          "id": "1907071256944-4118",
          "type": "single-select",
          "value": "Ghanaian by birth",
          "valueProps": {
            "key": "Ghanaian by birth",
            "level": 0,
            "value": "Ghanaian by birth"
          }
        },
        {
          "id": "1907071326595-4118",
          "type": "single-select",
          "value": "Ewe",
          "valueProps": {
            "key": "Ewe",
            "level": 0,
            "value": "Ewe"
          }
        },
        {
          "id": "1907071359051-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071355500-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071319802-4118",
          "type": "number",
          "value": "02"
        },
        {
          "id": "1907071301781-4118",
          "type": "single-select",
          "value": "Settlement",
          "valueProps": {
            "key": "Settlement",
            "level": 0,
            "value": "Settlement"
          }
        },
        {
          "id": "1907071334835-4118",
          "type": "single-select",
          "value": "ADANSI ASOKWA",
          "valueProps": {
            "key": "ADANSI ASOKWA ",
            "level": 0,
            "value": "ADANSI ASOKWA "
          }
        },
        {
          "id": "1907071339305-4118",
          "type": "single-select",
          "value": "No Religion",
          "valueProps": {
            "key": "No Religion",
            "level": 0,
            "value": "No Religion"
          }
        },
        {
          "id": "1907071412334-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071450687-4118",
          "type": "single-select",
          "value": "Past",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "Past"
          }
        },
        {
          "id": "1907071450366-4118",
          "type": "single-select",
          "value": "Primary",
          "valueProps": {
            "key": "Primary",
            "level": 0,
            "value": "Primary"
          }
        },
        {
          "id": "1907071536803-4118",
          "type": "single-select",
          "value": "P6",
          "valueProps": {
            "key": "P6",
            "level": 0,
            "value": "P6"
          }
        },
        {
          "id": "1907071553877-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071513292-4118",
          "type": "single-select",
          "value": "Worked before, seeking work and available for work",
          "valueProps": {
            "key": "Worked before, seeking work and available for work",
            "level": 0,
            "value": "Worked before, seeking work and available for work"
          }
        },
        {
          "id": "1907071605377-4118",
          "type": "text",
          "value": "sales"
        },
        {
          "id": "1907071629071-4118",
          "type": "text",
          "value": "quaci keys"
        },
        {
          "id": "1907071656964-4118",
          "type": "text",
          "value": "delivery service"
        },
        {
          "id": "1907071612406-4118",
          "type": "number",
          "value": "0006"
        },
        {
          "id": "1907071642158-4118",
          "type": "single-select",
          "value": "Casual worker",
          "valueProps": {
            "key": "Casual worker",
            "level": 0,
            "value": "Casual worker"
          }
        },
        {
          "id": "1907071601503-4118",
          "type": "single-select",
          "value": "Private Formal",
          "valueProps": {
            "key": "Private Formal",
            "level": 0,
            "value": "Private Formal"
          }
        },
        {
          "id": "1907071618184-4118",
          "type": "single-select",
          "value": "No difficulty",
          "valueProps": {
            "key": "No difficulty",
            "level": 0,
            "value": "No difficulty"
          }
        },
        {
          "id": "1907071658280-4118",
          "type": "single-select",
          "value": "Yes, some difficulty",
          "valueProps": {
            "key": "Yes, some difficulty",
            "level": 0,
            "value": "Yes, some difficulty"
          }
        },
        {
          "id": "1907071611525-4118",
          "type": "single-select",
          "value": "Cannot do at all",
          "valueProps": {
            "key": "Cannot do at all",
            "level": 0,
            "value": "Cannot do at all"
          }
        },
        {
          "id": "1907071612624-4118",
          "type": "single-select",
          "value": "No difficulty",
          "valueProps": {
            "key": "No difficulty",
            "level": 0,
            "value": "No difficulty"
          }
        },
        {
          "id": "1907071616195-4118",
          "type": "single-select",
          "value": "No difficulty",
          "valueProps": {
            "key": "No difficulty",
            "level": 0,
            "value": "No difficulty"
          }
        },
        {
          "id": "1907071646823-4118",
          "type": "single-select",
          "value": "Yes, some difficulty",
          "valueProps": {
            "key": "Yes, some difficulty",
            "level": 0,
            "value": "Yes, some difficulty"
          }
        },
        {
          "id": "1907071728298-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071701843-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071706898-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071718023-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071704025-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071759761-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071727530-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071720611-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071748490-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071806080-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071841755-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071853361-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071849150-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071840521-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071829216-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071834097-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071859063-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071836937-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071832480-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071833841-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071828633-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071812000-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071841783-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071810329-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071814593-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071812989-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071844163-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071856871-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071813993-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        }
      ]
    }
  ]
      },
      {
        "id": "1907071143959",
        "type": "calculation",
        "value": "2.0"
      },
      {
        "id": "1811271449856-4118",
        "type": "sub-form",
        "value": "1",
        "values": [
    {
      "UUID": "73e2c37c-45ff-4a67-858b-edd103331b18",
      "label": {
        "label1": "Male",
        "label2": "10"
      },
      "questions": [
        {
          "id": "1811271342192-1645",
          "type": "text",
          "value": "joel bee"
        },
        {
          "id": "1811271358713-1645",
          "type": "single-select",
          "value": "Other relative",
          "valueProps": {
            "key": "Other relative",
            "level": 0,
            "value": "Other relative"
          }
        },
        {
          "id": "1811271309487-1645",
          "type": "single-select",
          "value": "Male",
          "valueProps": {
            "key": "01",
            "level": 0,
            "value": "Male"
          }
        },
        {
          "id": "1811271358847-1645",
          "type": "number",
          "value": "10"
        },
        {
          "id": "1811271345143-1645",
          "type": "text",
          "value": "asakwa"
        },
        {
          "id": "1811271312830-1645",
          "type": "single-select",
          "value": "Upper East",
          "valueProps": {
            "key": "09",
            "level": 0,
            "value": "Upper East"
          }
        },
        {
          "id": "1811271317053-1645",
          "type": "number",
          "value": "8"
        }
      ]
    }
  ]
      },
      {
        "id": "1811301740481-4118",
        "type": "single-select",
        "value": "No",
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": "No"
  }
      },
      {
        "id": "1907071933179-4118",
        "type": "single-select",
        "value": "Yes",
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": "Yes"
  }
      },
      {
        "id": "1907071255047-4118",
        "type": "sub-form",
        "value": "1",
        "values": [
    {
      "UUID": "7a48b776-812e-46d3-be64-1813e0cbe9c7",
      "label": {
        "label1": "sheri koko",
        "label2": "Male"
      },
      "questions": [
        {
          "id": "1907071200479-4118",
          "type": "text",
          "value": "sheri koko"
        },
        {
          "id": "1907071911414-4118",
          "type": "single-select",
          "value": "Male",
          "valueProps": {
            "key": "Male",
            "level": 0,
            "value": "Male"
          }
        },
        {
          "id": "1907071944207-4118",
          "type": "number",
          "value": "08"
        },
        {
          "id": "1907071943409-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        }
      ]
    }
  ]
      },
      {
        "id": "1811301512710-4118",
        "type": "single-select",
        "value": "Container",
        "valueProps": {
    "key": "08",
    "level": 0,
    "value": "Container"
  }
      },
      {
        "id": "1811301554053-4118",
        "type": "single-select",
        "value": "Metal Sheet/ Slate/Asbestos",
        "valueProps": {
    "key": "3",
    "level": 0,
    "value": "Metal Sheet/ Slate/Asbestos"
  }
      },
      {
        "id": "1811301650944-4118",
        "type": "single-select",
        "value": "Mud/Mud bricks/Earth",
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": "Mud/Mud bricks/Earth"
  }
      },
      {
        "id": "1811301501767-4118",
        "type": "single-select",
        "value": "Earth/Mud",
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": "Earth/Mud"
  }
      },
      {
        "id": "1811301602768-4118",
        "type": "single-select",
        "value": "Squatting",
        "valueProps": {
    "key": "5",
    "level": 0,
    "value": "Squatting "
  }
      },
      {
        "id": "1811301630881-4118",
        "type": "single-select",
        "value": "Other private individual",
        "valueProps": {
    "key": "3",
    "level": 0,
    "value": "Other private individual "
  }
      },
      {
        "id": "1811301644966-4118",
        "type": "number",
        "value": "02"
      },
      {
        "id": "1907072022847",
        "type": "number",
        "value": "01"
      },
      {
        "id": "1811301747550-4118",
        "type": "single-select",
        "value": "Kerosene lamp",
        "valueProps": {
    "key": "03",
    "level": 0,
    "value": "Kerosene lamp"
  }
      },
      {
        "id": "1907082222502",
        "type": "single-select",
        "value": "Solar energy",
        "valueProps": {
    "key": "05",
    "level": 0,
    "value": "Solar energy"
  }
      },
      {
        "id": "1811301721966-4118",
        "type": "single-select",
        "value": "Rain water",
        "valueProps": {
    "key": "07",
    "level": 0,
    "value": "Rain water"
  }
      },
      {
        "id": "1907072006203-4118",
        "type": "number",
        "value": "12"
      },
      {
        "id": "1812031816883-4118",
        "type": "single-select",
        "value": "Protected well",
        "valueProps": {
    "key": "06",
    "level": 0,
    "value": "Protected well"
  }
      },
      {
        "id": "1812031842149-4118",
        "type": "single-select",
        "value": "LPG",
        "valueProps": {
    "key": "03",
    "level": 0,
    "value": "LPG"
  }
      },
      {
        "id": "1907072039543",
        "type": "single-select",
        "value": "Kerosene",
        "valueProps": {
    "key": "06",
    "level": 0,
    "value": "Kerosene"
  }
      },
      {
        "id": "1812031805453-4118",
        "type": "single-select",
        "value": "Bedroom/hall/living room",
        "valueProps": {
    "key": "5",
    "level": 0,
    "value": "Bedroom/hall/living room"
  }
      },
      {
        "id": "1812031858222-4118",
        "type": "single-select",
        "value": "Public bath house",
        "valueProps": {
    "key": "5",
    "level": 0,
    "value": "Public bath house "
  }
      },
      {
        "id": "1907072130271-4118",
        "type": "single-select",
        "value": "Covered container",
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": "Covered container"
  }
      },
      {
        "id": "1812031819558-4118",
        "type": "single-select",
        "value": "Public dump(container)",
        "valueProps": {
    "key": "Public dump(container)",
    "level": 0,
    "value": "Public dump(container)"
  }
      },
      {
        "id": "1907080643440-4118",
        "type": "single-select",
        "value": "KVIP/VIP",
        "valueProps": {
    "key": "03",
    "level": 0,
    "value": "KVIP/VIP"
  }
      },
      {
        "id": "1907080609573-4118",
        "type": "single-select",
        "value": "Satopan",
        "valueProps": {
    "key": "7",
    "level": 0,
    "value": "Satopan"
  }
      },
      {
        "id": "1907080606726-4118",
        "type": "number",
        "value": "02"
      },
      {
        "id": "1812031818838-4118",
        "type": "single-select",
        "value": "Yes, with other household(s) and located in different house",
        "valueProps": {
    "key": "Yes, with other household(s) and located in different house",
    "level": 0,
    "value": "Yes, with other household(s) and located in different house"
  }
      },
      {
        "id": "1907080720185-4118",
        "type": "single-select",
        "value": "Dumped indiscriminately",
        "valueProps": {
    "key": "6",
    "level": 0,
    "value": "Dumped indiscriminately"
  }
      },
      {
        "id": "1811301446111-4118",
        "type": "single-select",
        "value": "Fixed facility in yard/plot",
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": "Fixed facility in yard/plot"
  }
      },
      {
        "id": "1812041005257-4118",
        "type": "single-select",
        "value": "Yes, sometimes",
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": "Yes, sometimes"
  }
      },
      {
        "id": "1812041053633-4118",
        "type": "single-select",
        "value": "Through drains/gutter",
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": "Through drains/gutter"
  }
      }
    ],
    "start_date": "2019-07-30 17:03:29",
    "time_spent": "2388000",
    "version": "19073011592159"
  };

let data = jfaker.process(template);

console.log(JSON.stringify(template));