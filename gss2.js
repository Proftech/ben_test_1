var jfaker = require('json-faker');
var faker = require('faker');
var moment = require('moment');

function dataGenerator(){
    var regions = ["Ashanti","Greater Accra"];
var districts = {
    "Ashanti": ["Adansi South", "Adansi South"],
    "Greater Accra": ["Ablekuma", "Ada East"]
}
var regionIds = {
    "Ashanti": "1561633716958-9",
    "Greater Accra": "1561633716957-9"
}
var districtIds = {
    "Ashanti": ["1561633716958-9-1561633716958-2","1561633716958-9-1561633716958-2"],
    "Greater Accra": ["1561633716957-9-1561633716957-2","1561633716957-9-1561633716957-2"]
}
var randomRegion = Math.floor(Math.random() * Math.floor(regions.length));
var randomDistrict = Math.floor(Math.random() * Math.floor(districts[regions[randomRegion]].length))
var relationshiptoHH = faker.helpers.randomize(['Head','Spouse (Wife/Husband)','Child (Son/Daughter)','Parent/Parent in-law','Son/Daughter in-law','Grandchild','Brother/Sister','Step Child','Foster child','Other relative','Non-relative','Househelp (other relative)','Househelp (non-relative)','Group Ouarters/Outdoor Sleeper'])
var gender = faker.helpers.randomize(['Male','Female'])
var gender2 = faker.helpers.randomize(['Male','Female'])
var birthDate = faker.date.past()
var birthDate = moment(birthDate)
var birthYears = birthDate.diff(birthDate.diff(moment()))
var naturalization = faker.helpers.randomize(['Ghanaian by birth','Ghanaian by naturalization'])
var ethnicity = faker.helpers.randomize(['Ashanti','Fanti','Ewe','Ga','Dagbani','Other'])
boolYN = faker.helpers.randomize(['Yes','No'])

///////////////////////////////////////////////////////////////////CONSTANTS
const boolPositive = 'Yes'
const boolNegative = 'No'
const settlementReason = faker.helpers.randomize(['Employment','Settlement','MArriage/Family reunification','Education/Training','Asylum/Refugee','Conflict(war,ethnic,political,etc','Disaster(flood,drought,fire,etc','Other'])
const religion = faker.helpers.randomize(['No Religion','Catholic','Protestant(Anglican,Lutheran,Presbyterian,Methodist,etc)','Pentecostal/Charisnmatic','Other Christian','Islam','Ahmadi','Traditionalist','Other'])
const maritalStatus = faker.helpers.randomize(['Never married','Informal/consensual union/living together','Married','Separated','Other Christian','Divorced','Widowed'])
const literacy = faker.helpers.randomize(['Yes, read only','Yes, read and write','No'])
const everAttendedSch = faker.helpers.randomize(['Never','Now','Past'])
const eduLevel= faker.helpers.randomize(['Pre-school','P1','P2',"P3","P4","P5","P6","JHS1","JHS2","JHS3"])
const workTypeDone = faker.helpers.randomize(['Worked in economic activity, but received no payment','Did not work, but had job to go back to','Worked before, seeking work and available for work',"Seeking work for the first time and available for work","Did not work and not seeking work"])
const employeeStatus = faker.helpers.randomize(['Employee','Self-employed without employees','Self-employed with employees',"Casual worker","Contributing family worker","Paid apprentice","Unaid apprentice","Domestic employee(househelp, garden boy, etc)","Other"])
const sectorEmployed = faker.helpers.randomize(['Public (Government)','Semi-Public/Parastatal','Private Formal',"Private informal","NGO/CSO (Local)","NGO/CSO (International)","International Organization"])
const disabilityDiff1= faker.helpers.randomize(['No difficulty','Yes, some difficulty','Yes, a lot difficulty','Cannot do at all'])
const disabilityDiff2= faker.helpers.randomize(['No difficulty','Yes, some difficulty','Yes, a lot difficulty','Cannot do at all'])
const disabilityDiff3= faker.helpers.randomize(['No difficulty','Yes, some difficulty','Yes, a lot difficulty','Cannot do at all'])
const region = faker.helpers.randomize(['Ashanti','Brong Ahafo','Northern','Upper East','Upper West'])
const causeDeath = faker.helpers.randomize(['Yes, accident','Yes, violence','Yes, homicide','Yes, suicide','No'])
const dwellingType= faker.helpers.randomize(['Separate house','Semi-detached house','Flat/Apartment','Compound house','Huts/Building(same compound)','Huts/Building(different compound)','Tent','Container','Wooden/Kiosk','Living quaters attached to office/shop','Uncompleted building','Other'])
const wallMaterial= faker.helpers.randomize(['Mud/Mud bricks/Earth','Wood','Metal Sheet/Absestos','Stone','Burnt bricks','Cement blocks/Concrete','Tent','Landcrete','Bamboo','Palm leaves/Thatch(Grass/Raffia)','Other'])
const roofMaterial= faker.helpers.randomize(['Mud/Mud bricks/Earth','Wood','Metal Sheet/Absestos','Stone','Burnt bricks','Cement blocks/Concrete','Tent','Landcrete','Bamboo','Palm leaves/Thatch(Grass/Raffia)','Other'])

const floorMaterial= faker.helpers.randomize(['Earth/Mud','Wood','Burnt bricks','Carpet','Burnt bricks','Cement/Concrete','Vinyl tiles','Ceramic/Porcelain/Granite/Marble tiles'])
const tenancyAgree= faker.helpers.randomize(['Owner occupied','Renting','Rent-free','Perching','Squatting','Caretaker','Other'])
const houseOwner= faker.helpers.randomize(['Estate Developer','Relative not household member','Private Employer','Othe private agency','Public/Goverment ownership','Other'])
const light1= faker.helpers.randomize(['Electricity(mains)','Electricity( private mgenerator)','Kerosene lamp','Gas lamp','Solar energy','Candle','Flashlight/Torch','Candle','Firewood','Crop residue','Other','Electricity (local mini grid)'])
const light2= faker.helpers.randomize(['Electricity(mains)','Electricity( private mgenerator)','Kerosene lamp','Gas lamp','Solar energy','Candle','Flashlight/Torch','Candle','Firewood','Crop residue','Other','Electricity (local mini grid)'])
const drinkSource1= faker.helpers.randomize(['Pipe-borne inside dwelling','Pipe-borne outside dwelling but not on compound','Pipe-borne outside dwelling but from neighbors house','Public tap/Stand pip','Bore-hole/Pump/Tube well','Protected well','Rain water','Protected spring','Bottled water','Sachet water','Tanker supply/Vendor provided','Unprotected well','Unprotected spring','River/Stream','Dugout/Pond/Lake/Dam/Canal','Other'])
const cookFuel= faker.helpers.randomize(['Other','Wood','LPG','Bio Gas','Electricity','Kerosene','Chacoal','Crop Residual','Animal waste','Cooking gel','Other'])
const cookFuel1= faker.helpers.randomize(['Other','Wood','LPG','Bio Gas','Electricity','Kerosene','Chacoal','Crop Residual','Animal waste','Cooking gel','Other'])
const cookSpace= faker.helpers.randomize(['Separate room for exclusive use of household','Separate room shared with other household(s)','Enclosure without roof','Structure with roof but without walls','Bedroom/hall/living room','Veranda','Open space in compound','Other'])
const bathroomFacility= faker.helpers.randomize(['Owned bathrrom for exclusive use','Shared separate bathroom in same house','Private open cubicle','Shared open cubicle','Public bath house ','Bathroom in another house','Open space around house','In a river, pond, lake or dam','Other'])
const refuseBinType= faker.helpers.randomize(['Standard waste bin','Covered container','Uncovered container','Covered/uncovered basket','Sack','Polythene bag','Other'])
const disposeRubbish= faker.helpers.randomize(['Collected','Burned by household','Public dump(container)','Public dump (open space)','Dumped indiscriminately ','Buried by household','Other'])
const toiletFacility= faker.helpers.randomize(['No toilet facility','Septic tank (manhole)','KVIP/VIP','Pit latrine','Enviro Loo ','Bio-digester (e.g. bio fill)','Bio gas','Bucket/Pan','Portable toilet (e.g. Water potti)','Sewer','Public toilet','Other'])
const toiletHole= faker.helpers.randomize(['WC seat','WC squat-hole','Pour-flush bowl','Urine-diverting dry toilet (UDDT)','Concrete pedestal/slab','Wooden pedestal/slab','Satopan','Micro flush','Other'])
const shareToilet= faker.helpers.randomize(['Yes, with other household(s) in same house','Yes, with other household(s) in different house','Yes, with other household(s) and located in different house','No'])
const toiletDisposed = faker.helpers.randomize(['Burn','Flush/drop','Anal wash','Waste receptacle (bin, sac, polythene, etc.)','Bury in the ground','Dumped indiscriminately','Other'])
const whereHandsWashed = faker.helpers.randomize(['Fixed facility in dwelling','Fixed facility in yard/plot','Mobile object (bucket/jug/kettle)','Mobile object (bucket/jug/kettle)'])
const HandsWashedrunningWater = faker.helpers.randomize(['Yes,always','Yes,something','No'])
const wasterWayerDisposed = faker.helpers.randomize(['Through the sewerage system','Through drains/gutter','Through drainage into a pit (soak away)','Thrown onto the ground/street/outside','Other'])


//var birthYears = duration.asYears();
//var randomDistrictId = Math.floor(Math.random() * Math.floor(districtIds[regionsIds[randonRegion]].length))
//var randomRegionId = Math.floor(Math.random() * Math.floor(regionIds.length));

let template = {
    "UUID": faker.random.uuid(),
    "device": {
      "apiLevel": 22,
      "app_version": "v1.1.75-Demo",
      "batterylevel": 71.0,
      "brand": "samsung",
      "freeDisk": "4,917MB",
      "freeMemory": "432MB",
      "imei_number": "352948092905516",
      "locale": "en_GB",
      "locationStatus": "Enabled",
      "manufacturer": "samsung",
      "model": "SM-T285",
      "networkAccess": "wifi",
      "orientation": "Portrait",
      "osVersion": "5.1.1",
      "osVersionName": "LOLLIPOP_MR1"
    },
    "end_date": "2019-07-30 17:03:45",
    "formID": "11032",
    "geofence": "",
    "gis": false,
    "gis_data": "",
    "label": {
      "label1": "03",
      "label2": "no_res_!@#$%"
    },
    "original_gis_data": "",
    "questions": [
      {
        "id": "1811271121715-4118",
        "type": "multi-text",
        "value": faker.address.streetAddress(true)
      },
      {
        "id": "1907222036486-4118",
        "type": "text",
        "value": faker.helpers.randomize(['A','S','D','F','G'])+faker.helpers.randomize(['A','S','D','F','G'])+"-"+faker.random.number({min:100,max:999})+"-"+faker.random.number({min:1000,max:9999})
      },
      {
        "id": "1811271146097-4118",
        "type": "text",
        "value": faker.helpers.randomize(['024','054','055','020','050']) + faker.random.number({min:1000000, max:9999999})
      },
      {
        "id": "1811271131488-4118",
        "type": "text",
        "value": faker.helpers.randomize(['024','054','055','020','050']) + faker.random.number({min:1000000, max:9999999})
      },
      {
        "id": "1811271141620-4118",
        "type": "option-tree",
        "value": districts[regions[randomRegion]][randomDistrict],
        "values": [{
            "heading_id": "1811271141620-4118-0",
            "id": "1561633716957-9",
            "key": regions[randomRegion],
            "level": 1,
            "name": regions[randomRegion]
          },
          {
            "heading_id": "1811271141620-4118-1",
            "id": "1561633716957-9-1561633716957-2",
            "key": districts[regions[randomRegion]][randomDistrict],
            "level": 2,
            "name": districts[regions[randomRegion]][randomDistrict]
          }]
      },
      {
        "id": "1907261558769-4118",
        "type": "single-select",
        "value": faker.helpers.randomize(['Type 1','Type 2','Type 3']),
        "valueProps": {
    "key": faker.helpers.randomize(['Type 1','Type 2','Type 3']),
    "level": 0,
    "value": "Type 3"
  }
      },
      {
        "id": "1811271111355-4118",
        "type": "text",
        "value": faker.random.number({min:10,max:99})
      },
      {
        "id": "1907241811782-4118",
        "type": "text",
        "value": faker.random.number({min:100,max:999})
      },
      {
        "id": "1907052023733-4118",
        "type": "single-select",
        "value": "Occupied housing unit",
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": "Occupied housing unit"
  }
      },
      {
        "id": "1907261413321-4118",
        "type": "text",
        "value": faker.random.number({min:100,max:999})
      },
      {
        "id": "1907082121107-4118",
        "type": "text",
        "value": faker.random.number({min:10,max:99})
      },
      {
        "id": "1907240909625-4118",
        "type": "gis",
        "value": faker.address.longitude() +',' +  faker.address.latitude() + ',' + faker.random.number({min:10,max:15})
      },
      {
        "id": "1907290827029-4118",
        "type": "number",
        "value": faker.helpers.randomize(['1','2','3','4','5']),
      },
      {
        "id": "1907052100650-4118",
        "type": "number",
        "value": faker.random.number({min:1,max:9})
      },
      {
        "id": "1907071145131-4118",
        "type": "number",
        "value": faker.random.number({min:1,max:9})
      },
      {
        "id": "1907071152192",
        "type": "number",
        "value": faker.random.number({min:1,max:4})
      },
      {
        "id": "1907071157304",
        "type": "number",
        "value": faker.random.number({min:1,max:4})
      },
      {
        "id": "1907071140561",
        "type": "number",
        "value": faker.random.number({min:1,max:2})
      },
      {
        "id": "1907071107856",
        "type": "number",
        "value": faker.random.number({min:1,max:4})
      },
      {
        "id": "1907071151393-4118",
        "type": "calculation",
        "value": "4.0"
      },
      {
        "id": "1811271114013-4118",
        "type": "sub-form",
        "value": "1",
        "values": [
    {
      "UUID": "c5c8b510-2b50-4f9f-9416-cf3f61508a1d",
      "label": {
        "label1": "Aisha bee",
        "label2": "Parent/Parent in-law"
      },
      "questions": [
        {
          "id": "1811271103831-1643",
          "type": "text",
          "value": faker.name.firstName()+' '+faker.name.lastName()
        },
        {
          "id": "1811271242384-1643",
          "type": "single-select",
          "value": relationshiptoHH,
          "valueProps": {
            "key": relationshiptoHH,
            "level": 0,
            "value": relationshiptoHH,
          }
        },
        {
          "id": "1811271240295-1643",
          "type": "single-select",
          "value": gender,
          "valueProps": {
            "key": gender,
            "level": 0,
            "value": gender
          }
        },
        {
          "id": "1812041627200-1643",
          "type": "single-select",
          "value": "Visitor(s) present on census night",
          "valueProps": {
            "key": "B",
            "level": 0,
            "value": "Visitor(s) present on census night"
          }
        },
        {
          "id": "1907071258306-4118",
          "type": "date",
          "value": birthDate
        },
        {
          "id": "1907071227083-4118",
          "type": "number",
          "value": birthYears
        },
        {
          "id": "1907071256944-4118",
          "type": "single-select",
          "value": naturalization,
          "valueProps": {
            "key": naturalization,
            "level": 0,
            "value": naturalization
          }
        },
        {
          "id": "1907071326595-4118",
          "type": "single-select",
          "value": ethnicity,
          "valueProps": {
            "key": ethnicity,
            "level": 0,
            "value": ethnicity
          }
        },
        {
          "id": "1907071359051-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071355500-4118",
          "type": "single-select",
          "value": boolNegative,
          "valueProps": {
            "key": boolNegative,
            "level": 0,
            "value": boolPositive
          }
        },
        {
          "id": "1907071319802-4118",
          "type": "number",
          "value": faker.random.number({min:10,max:99})
        },
        {
          "id": "1907071301781-4118",
          "type": "single-select",
          "value": settlementReason ,
          "valueProps": {
            "key": settlementReason ,
            "level": 0,
            "value": settlementReason 
          }
        },
        {
          "id": "1907071334835-4118",
          "type": "single-select",
          "value": districts[regions[randomRegion]][randomDistrict],
          "valueProps": {
            "key": districts[regions[randomRegion]][randomDistrict],
            "level": 0,
            "value": districts[regions[randomRegion]][randomDistrict]
          }
        },
        {
          "id": "1907071339305-4118",
          "type": "single-select",
          "value": religion,
          "valueProps": {
            "key": religion,
            "level": 0,
            "value": religion
          }
        },
        {
            "id": "1907071317360-4118",
            "type": "single-select",
            "value": maritalStatus,
            "valueProps": {
              "key": maritalStatus,
              "level": 0,
              "value": maritalStatus
            }
          },
        {
          "id": "1907071412334-4118",
          "type": "single-select",
          "value": literacy,
          "valueProps": {
            "key": literacy,
            "level": 0,
            "value": literacy
          }
        },
        
        {
          "id": "1907071450687-4118",
          "type": "single-select",
          "value": everAttendedSch,
          "valueProps": {
            "key": everAttendedSch,
            "level": 0,
            "value": everAttendedSch
          }
        },
        {
          "id": "1907071450366-4118",
          "type": "single-select",
          "value": "JSS/JHS",
          "valueProps": {
            "key": "JSS/JHS",
            "level": 0,
            "value": "JSS/JHS"
          }
        },
        {
          "id": "1907071536803-4118",
          "type": "single-select",
          "value": eduLevel,
          "valueProps": {
            "key": eduLevel,
            "level": 0,
            "value": eduLevel
          }
        },
        {
          "id": "1907071553877-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071513292-4118",
          "type": "single-select",
          "value": "Worked before, seeking work and available for work",
          "valueProps": {
            "key": "Worked before, seeking work and available for work",
            "level": 0,
            "value": "Worked before, seeking work and available for work"
          }
        },
        {
          "id": "1907071605377-4118",
          "type": "text",
          "value": faker.name.jobType()
        },
        {
          "id": "1907071629071-4118",
          "type": "text",
          "value": faker.name.jobTitle()
        },
        {
          "id": "1907071656964-4118",
          "type": "text",
          "value": faker.name.jobTitle()
        },
        {
          "id": "1907071612406-4118",
          "type": "number",
          "value": faker.random.number({min:1000,max:9999})
        },
        {
          "id": "1907071642158-4118",
          "type": "single-select",
          "value": employeeStatus,
          "valueProps": {
            "key": employeeStatus,
            "level": 0,
            "value": employeeStatus
          }
        },
        {
          "id": "1907071601503-4118",
          "type": "single-select",
          "value": sectorEmployed,
          "valueProps": {
            "key": sectorEmployed,
            "level": 0,
            "value": sectorEmployed
          }
        },
        {
          "id": "1907071618184-4118",
          "type": "single-select",
          "value": disabilityDiff1,
          "valueProps": {
            "key": disabilityDiff1,
            "level": 0,
            "value": disabilityDiff1
          }
        },
        {
          "id": "1907071658280-4118",
          "type": "single-select",
          "value": disabilityDiff2,
          "valueProps": {
            "key": disabilityDiff2,
            "level": 0,
            "value": disabilityDiff2
          }
        },
        {
          "id": "1907071611525-4118",
          "type": "single-select",
          "value": disabilityDiff2,
          "valueProps": {
            "key": disabilityDiff2,
            "level": 0,
            "value": disabilityDiff2
          }
        },
        {
          "id": "1907071612624-4118",
          "type": "single-select",
          "value": disabilityDiff1,
          "valueProps": {
            "key": disabilityDiff1,
            "level": 0,
            "value": disabilityDiff1
          }
        },
        {
          "id": "1907071616195-4118",
          "type": "single-select",
          "value": disabilityDiff2,
          "valueProps": {
            "key": disabilityDiff2,
            "level": 0,
            "value": disabilityDiff2
          }
        },
        {
          "id": "1907071646823-4118",
          "type": "single-select",
          "value": disabilityDiff3,
          "valueProps": {
            "key": disabilityDiff3,
            "level": 0,
            "value": disabilityDiff3
          }
        },
        {
          "id": "1907071728298-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071701843-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071706898-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071718023-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071704025-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071759761-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071727530-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071720611-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071748490-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071806080-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071841755-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071853361-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071849150-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071840521-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071829216-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071834097-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071859063-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071836937-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071832480-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071833841-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071828633-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071812000-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071841783-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071810329-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071814593-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071812989-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071844163-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        },
        {
          "id": "1907071856871-4118",
          "type": "single-select",
          "value": "No",
          "valueProps": {
            "key": "No",
            "level": 0,
            "value": "No"
          }
        },
        {
          "id": "1907071813993-4118",
          "type": "single-select",
          "value": "Yes",
          "valueProps": {
            "key": "Yes",
            "level": 0,
            "value": "Yes"
          }
        }
      ]
    }
  ]
      },
      {
        "id": "1907071143959",
        "type": "calculation",
        "value": "2.0"
      },
      {
        "id": "1811271449856-4118",
        "type": "sub-form",
        "value": "1",
        "values": [
    {
      "UUID": "73e2c37c-45ff-4a67-858b-edd103331b18",
      "label": {
        "label1": "Male",
        "label2": "10"
      },
      "questions": [
        {
          "id": "1811271342192-1645",
          "type": "text",
          "value": faker.name.firstName()+' '+faker.name.lastName()
        },
        {
          "id": "1811271358713-1645",
          "type": "single-select",
          "value": relationshiptoHH,
          "valueProps": {
            "key": relationshiptoHH,
            "level": 0,
            "value": relationshiptoHH
          }
        },
        {
          "id": "1811271309487-1645",
          "type": "single-select",
          "value": gender,
          "valueProps": {
            "key": "01",
            "level": 0,
            "value": gender
          }
        },
        {
          "id": "1811271358847-1645",
          "type": "number",
          "value": faker.random.number({min:10,max:66})
        },
        {
          "id": "1811271345143-1645",
          "type": "text",
          "value": faker.address.streetName()
        },
        {
          "id": "1811271312830-1645",
          "type": "single-select",
          "value": region,
          "valueProps": {
            "key": "09",
            "level": 0,
            "value": region
          }
        },
        {
          "id": "1811271317053-1645",
          "type": "number",
          "value": faker.random.number({min:1,max:12})
        }
      ]
    }
  ]
      },
      {
        "id": "1811301740481-4118",
        "type": "single-select",
        "value": "No",
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": "No"
  }
      },
      {
        "id": "1907071933179-4118",
        "type": "single-select",
        "value": "Yes",
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": "Yes"
  }
      },
      {
        "id": "1907071255047-4118",
        "type": "sub-form",
        "value": "1",
        "values": [
    {
      "UUID": "7a48b776-812e-46d3-be64-1813e0cbe9c7",
      "label": {
        "label1":faker.name.firstName() + ' ' + faker.name.lastName(),
        "label2": gender
      },
      "questions": [
        {
          "id": "1907071200479-4118",
          "type": "text",
          "value": faker.name.firstName() + ' ' + faker.name.lastName()
        },
        {
          "id": "1907071911414-4118",
          "type": "single-select",
          "value": gender2,
          "valueProps": {
            "key": gender2,
            "level": 0,
            "value": gender2
          }
        },
        {
          "id": "1907071944207-4118",
          "type": "number",
          "value": faker.random.number({min:23,max:69})
        },
        {
          "id": "1907071943409-4118",
          "type": "single-select",
          "value": causeDeath,
          "valueProps": {
            "key": causeDeath,
            "level": 0,
            "value": causeDeath
          }
        }
      ]
    }
  ]
      },
      {
        "id": "1811301512710-4118",
        "type": "single-select",
        "value": dwellingType,
        "valueProps": {
    "key": "08",
    "level": 0,
    "value": dwellingType
  }
      },
      {
        "id": "1811301554053-4118",
        "type": "single-select",
        "value": wallMaterial,
        "valueProps": {
    "key": "3",
    "level": 0,
    "value": wallMaterial
  }
      },
      {
        "id": "1811301650944-4118",
        "type": "single-select",
        "value": roofMaterial,
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": roofMaterial
  }
      },
      {
        "id": "1811301501767-4118",
        "type": "single-select",
        "value": floorMaterial,
        "valueProps": {
    "key": "1",
    "level": 0,
    "value": floorMaterial
  }
      },
      {
        "id": "1811301602768-4118",
        "type": "single-select",
        "value": tenancyAgree
         ,
        "valueProps": {
    "key": "5",
    "level": 0,
    "value": tenancyAgree
  }
      },
      {
        "id": "1811301630881-4118",
        "type": "single-select",
        "value": houseOwner,
        "valueProps": {
    "key": "3",
    "level": 0,
    "value": houseOwner
  }
      },
      {
        "id": "1811301644966-4118",
        "type": "number",
        "value": faker.random.number({min:1,max:20})
      },
      {
        "id": "1907072022847",
        "type": "number",
        "value": faker.random.number({min:1,max:20})
      },
      {
        "id": "1811301602706-4118",
        "type": "single-select",
        "value": boolNegative,
        "valueProps" : {
            "key": boolNegative,
            "level": 0,
            "value": boolNegative
          }
      },
      {
        "id": "1811301747550-4118",
        "type": "single-select",
        "value": light1,
        "valueProps": {
    "key": "03",
    "level": 0,
    "value": light1
  }
      },
      {
        "id": "1907082222502",
        "type": "single-select",
        "value": light2,
        "valueProps": {
    "key": "05",
    "level": 0,
    "value": light2
  }
      },
      {
        "id": "1811301721966-4118",
        "type": "single-select",
        "value": drinkSource1,
        "valueProps": {
    "key": "07",
    "level": 0,
    "value": drinkSource1
  }
      },
      {
        "id": "1907072006203-4118",
        "type": "number",
        "value": faker.random.number({min:20,max:120})
      },
      {
        "id": "1812031816883-4118",
        "type": "single-select",
        "value": drinkSource1,
        "valueProps": {
    "key": "06",
    "level": 0,
    "value": drinkSource1
  }
      },
      {
        "id": "1812031842149-4118",
        "type": "single-select",
        "value": cookFuel,
        "valueProps": {
    "key": cookFuel,
    "level": 0,
    "value": cookFuel
  }
      },
      {
        "id": "1907072039543",
        "type": "single-select",
        "value": cookFuel1,
        "valueProps": {
    "key": cookFuel1,
    "level": 0,
    "value": cookFuel1
  }
      },
      {
        "id": "1812031805453-4118",
        "type": "single-select",
        "value": cookSpace,
        "valueProps": {
    "key": cookSpace,
    "level": 0,
    "value": cookSpace
  }
      },
      {
        "id": "1812031858222-4118",
        "type": "single-select",
        "value": bathroomFacility,
        "valueProps": {
    "key": bathroomFacility,
    "level": 0,
    "value": bathroomFacility
  }
      },
      {
        "id": "1907072130271-4118",
        "type": "single-select",
        "value": refuseBinType,
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": refuseBinType
  }
      },
      {
        "id": "1812031819558-4118",
        "type": "single-select",
        "value": disposeRubbish,
        "valueProps": {
    "key": disposeRubbish,
    "level": 0,
    "value": disposeRubbish
  }
      },
      {
        "id": "1907080643440-4118",
        "type": "single-select",
        "value": toiletFacility,
        "valueProps": {
    "key": toiletFacility,
    "level": 0,
    "value": toiletFacility
  }
      },
      {
        "id": "1907080609573-4118",
        "type": "single-select",
        "value":toiletHole,
        "valueProps": {
    "key": toiletHole,
    "level": 0,
    "value": toiletHole
  }
      },
      {
        "id": "1907080606726-4118",
        "type": "number",
        "value": faker.helpers.randomize(['01','02','03','04','05'])
      },
      {
        "id": "1812031818838-4118",
        "type": "single-select",
        "value": shareToilet,
        "valueProps": {
    "key": shareToilet,
    "level": 0,
    "value": shareToilet
  }
      },
      {
        "id": "1907080729947-4118",
        "type": "number",
        "value": faker.helpers.randomize(['01','02','03','04','05'])
      },
      {
        "id": "1907080720185-4118",
        "type": "single-select",
        "value": toiletDisposed,
        "valueProps": {
    "key": "6",
    "level": 0,
    "value": toiletDisposed
  }
      },
      {
        "id": "1811301446111-4118",
        "type": "single-select",
        "value": whereHandsWashed,
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": whereHandsWashed
  }
      },
      {
        "id": "1812041005257-4118",
        "type": "single-select",
        "value": HandsWashedrunningWater,
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": HandsWashedrunningWater
  }
      },
      {
        "id": "1812041053633-4118",
        "type": "single-select",
        "value": wasterWayerDisposed,
        "valueProps": {
    "key": "2",
    "level": 0,
    "value": wasterWayerDisposed
  }
      }
    ],
    "start_date": "2019-07-30 17:03:29",
    "time_spent": "2388000",
    "version": "19073011592159"
  };

//let data = jfaker.process(template);
let data = JSON.stringify(template);


return data;

}

//console.log(data);
//console.log(dataGenerator());
module.exports= {dataGenerator}
